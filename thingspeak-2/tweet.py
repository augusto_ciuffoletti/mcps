import httplib, urllib, json, sys
if len(sys.argv) != 3:            # Controllo parametri
  print("\nUsage: "+sys.argv[0]+" <status> <apikey>\n")
  exit(1)
document = {"api_key":sys.argv[2],"status":sys.argv[1]}
conn = httplib.HTTPSConnection("api.thingspeak.com")      # Apro la connessione
conn.request(
  "POST",                                                 # metodo HTTP
  "/apps/thingtweet/1/statuses/update.json",              # URL
  json.JSONEncoder().encode(document),                    # HTTP body
  {"Content-type": "application/json"}                    # HTTP header
)
response = conn.getresponse()
print response.status, response.reason
print response.read()
conn.close()
