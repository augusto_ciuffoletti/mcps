import httplib, urllib, json, sys
if len(sys.argv) != 3:            # Controllo parametri
  print("\nUsage: "+sys.argv[0]+" <data> <apikey>\n")
  exit(1)
document = {"text":sys.argv[1],"author":"augusto"}
conn = httplib.HTTPSConnection("api.mlab.com")                      # Apro la connessione
conn.request(
  "POST",                                                           # metodo HTTP
  "/api/1/databases/example/collections/dati?apiKey="+sys.argv[2],  # URL
  json.JSONEncoder().encode(document),                                                    # HTTP body
  {"Content-type": "application/json"}                              # HTTP header
)
response = conn.getresponse()
print response.status, response.reason
print response.read()
conn.close()
