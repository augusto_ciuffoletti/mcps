import httplib, json, sys
if len(sys.argv) != 2:            # Controllo parametri
  print("\nUsage: "+sys.argv[0]+" <apikey>\n")
  exit(1)
conn = httplib.HTTPSConnection("api.mlab.com")  # Apro la connessione
# Invio la request
conn.request(
  "GET",                                        # metodo HTTP
  "/api/1/databases?apiKey="+sys.argv[1]        # HTTP header
)
response = conn.getresponse()
print response.status, response.reason
print response.read()
conn.close()
